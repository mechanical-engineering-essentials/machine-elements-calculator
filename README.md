# Machine Elements Calculator
Welcome to Machine Elements Calculator (M.E.C.) v1.1. 
This is a script that facilitates the automation of calculations accodring to DIN 743:2000.

More specifically in this version it can determine the safety factors (both Dynamic and Static) of a:
1)Gradation
2)Groove
3)Spline
4)Multispline
5)Clamp
6)more to be added in the future

M.E.C. is built solely using the basic python library. 

Written by Pavlidis Dimitrios, Mechanical Engineering Undergrad at AUTh.


## Installation
Both the latest python source code and an executable are available to download. Here's also a virustotal link of the executable:

https://www.virustotal.com/gui/file/07acd13b466eea3c58eca8bd1ce1982626c0004514650410d5936750c7ae886f?nocache=1 

## Usage
A showcase example:

Starting input window:
Fill in the blakc spaces with values according to what is being asked. Stresses shall be in MPa (or N/mm^2), dimensions shall be in mm.
Check the appropriate boxes according to the configuration of the shaft you want to calculate. 
At the very end, select the tick boxes (between "Tension/Compression", "Bending" "Torsion"), depending on what you want to calculate.

Results (for spline) will be displayed at the command window:

Kv equals 1
σ_mv equals 151.0
τ_mv equals 87.1798906476335

CALCULATING SPLINE

CALCULATING bending

K1 (plastic limit) equals 0.9898813028321038
K1 equals 1
σ_b(d) equals 360.0
βσ_zd,b(d_BK) equals 1.7400985274099126
d_BK equals 40
K3_zd,b equals 0.948731980318986
K3(d_BK)_zd,b equals 0.9380929049782927
βσ_zd,b(d) equals 1.7205850718530344
K2_zd,b equals 0.9074487147360963
Kfσ equals 1
Kσ equals 1.896068663619645
K2F_b equals 1.2
γ_Fzd,b equals 1.05
σ_bWK equals 94.93327085337471
σ_bFK equals 293.10385376858596
ψ_bσΚ equals 0.151877017967318

Field 2
σ_bADK equals 71.99984114030968

Sf equals 1.314367057258233

Sd equals 2.04139044911567

Critical Safety Factor: 1.314367057258233

## Support
Email me for any questions, requests, ideas or contributions at:

pavlidisdim02@gmail.com

## Roadmap
Future implementations may include:
1)Retaining Ring Groove calculations
3) Graded Groove calculations
2)Roller Chain/ Sprocket analysis and dimensioning
4)

## Authors and acknowledgment
A big thank you to all my friends that helped me test the code was working properly:
Asteris
Foivos
Nikos 
Giannis

## License
Still don't know what lisence to use. Project is open-source of course.

## Project status
Project gets updated on random time intervals.
